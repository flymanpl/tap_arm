#!/bin/bash
# Script that installs and configures NGINX+PM2
# NGINX will listen on port 80 and PM2 on port 3000
# Run this script as root
# By esa.hakala@nordcloud.com
# 27.01.2019

# Prepare directories, install required binaries
mkdir -p /var/www

apt-get update --fix-missing
apt-get -y install nginx npm

npm install pm2 -g

# Black magic to generate NGINX virtual host configuration
cat > /etc/nginx/sites-available/api <<EOF
upstream my_nodejs_upstream {
    server 127.0.0.1:3000;
    keepalive 64;
} 
server {
    listen 80;
    server_name my_nodejs_server;
    root /var/www/api;

    location / {
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header Host \$http_host;
        proxy_set_header X-NginX-Proxy true;
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_max_temp_file_size 0;
        proxy_pass http://my_nodejs_upstream/;
        proxy_redirect off;
        proxy_read_timeout 240s;
        proxy_connect_timeout 240s;
    }
}
EOF

# Create symlink to enable the site above
ln -s /etc/nginx/sites-available/api /etc/nginx/sites-enabled/api

# Remove NGINX "default" site
rm /etc/nginx/sites-enabled/default

# Make nginx start on boot and restart it after above config changes
systemctl enable nginx
systemctl restart nginx

# Get the api files from Esa's WebServer. No promises on SLA!
# TO DO: Create a new user, chown the files to it and run the app with some sane user
cd /var/www
curl https://bitbucket.org/flymanpl/tap_arm/raw/master/artifacts/api.tar.gz --output api.tar.gz
tar -xvf api.tar.gz
rm api.tar.gz
cd /var/www/api

# Install app's dependencies and run the app
# TO DO: Limit port 3000 only to localhost
npm install
PORT='3000' MONGODB_URI="mongodb://$2:$3@$1:27017/webratings" pm2 start /var/www/api/bin/www --name rating-api

# Make app run on boot
pm2 startup
pm2 save
